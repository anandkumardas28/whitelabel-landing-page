function isInArray(value, array) {
    return array.indexOf(value) > -1;
}

var BufferStatus = {

    seenStates: [],

    initBuffer: function () {
        if(Timeline.SettingsJsonObject.BufferBarEnabled){
            BufferStatus.createBufferDiv();
        }
    },

    createBufferDiv: function () {
        var counter = 1;

        var bufferSectionHTMLTemplate =
            '<div id="jsSection" class="buffer-section"> \
                <div id="jsSectionInner" class="buffer-section-inner"></div> \
            </div>';

        var bufferTimeline = $('#jsBufferTimeline');
        $('.timeline-state').each(function () {
            var stateTemplate = $(bufferSectionHTMLTemplate).clone();
            var stateName = $(this).data('state');
            var stateWidth = $(this).data('percent-width');

            stateTemplate.attr('id','jsSection'+counter);
            stateTemplate.data('interaction-id',stateName);
            stateTemplate.find('.buffer-section-inner').attr('id','jsSectionInner' + stateName);
            stateTemplate.css('width',stateWidth + "%");
            bufferTimeline.append(stateTemplate);

            counter++;
        });
        BufferStatus.applyBufferSettings();
    },

    applyBufferSettings: function () {
        var opacity = Timeline.SettingsJsonObject.BufferBarOpacity;
        var color = Timeline.SettingsJsonObject.BufferBarColor;
        var bgImage = Timeline.SettingsJsonObject.BufferBarImagePath;
        var bgImagePos = Timeline.SettingsJsonObject.BufferBarImagePosition;
        $('.buffer-section-inner').css({
            "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity="+ (opacity*100) +")", /* IE8 */
            "filter": "alpha(opacity=50)", /* IE5-7 */
            "-moz-opacity": opacity,
            "-khtml-opacity": opacity,
            "opacity": opacity
        });
        if(bgImage) {
            if(bgImagePos){
                $('.buffer-section-inner').css({
                    "background": "url(" + bgImage + ") repeat-x " + bgImagePos,
                });
            } else {
                $('.buffer-section-inner').css({
                    "background": "url(" + bgImage + ") repeat-x bottom",
                });
            }
        } else {
            $('.buffer-section-inner').css('background', color);
        }
    },

    updateBuffer: function () {
        if(LanguageSelector.currentLanguageObj !== {}){
            try {
                var videoTimes = VideoPlayerInterface.iframeWindow.rtc.player.getVideoTimes();
                var currentState = Timeline.getStateFromProgress(true);
                var bufferPercentage = Math.round(videoTimes.bufferedPercentage);


                //Checks if the video has reached the end and prevents the introduction buffer bar loading
                if(Timeline.getProgress() !== 1){
                    if(isInArray(currentState,BufferStatus.seenStates)){
                        BufferStatus.updateStateBufferProgress(currentState, bufferPercentage);
                    }
                }
                if(!isInArray(currentState,BufferStatus.seenStates)){
                    BufferStatus.seenStates.push(currentState);
                }
                BufferStatus.clearOldBuffers(currentState);
            } catch (e) {}
        }
    },

    clearOldBuffers: function (currentState) {
        for (var i=0; i < BufferStatus.seenStates.length; i++){
            var state = BufferStatus.seenStates[i];
            if(state !== currentState) {
                BufferStatus.updateStateBufferProgress(state,0);
            }
        }
    },

    //State is the id of the interaction card as a string.
    updateStateBufferProgress: function(state, percentage) {
        if (typeof state === "string"){
            $('#jsSectionInner'+state).css('width', percentage + "%");
        }
    }

};